import { AxiosResponse } from "axios";
import * as React from "react";
import { useEffect, useRef, useState } from "react";
import {
  Button,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  View
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { RouteComponentProps } from "react-router";
import { CheckBox } from "../../components/CheckBox";
import { LOGIN_ENDPOINT } from "../../constants/endpoints";
import { RootStoreContext } from "../../stores/RootStore";
import * as Request from "../../utils/Request";

interface Props extends RouteComponentProps {}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%"
  },
  loginBox: {
    backgroundColor: "rgba(230, 230, 230, 0.8)",
    padding: 6,
    width: "80%",
    maxWidth: 300,
    height: 350,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 10
  },
  logo: {
    width: 140,
    height: 111
  },
  inputContainer: {
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 3,
    width: "90%",
    borderColor: "#bbb",
    flexDirection: "row",
    height: 30
  },
  inputLogo: {
    height: "100%",
    backgroundColor: "#bbb",
    borderRightWidth: StyleSheet.hairlineWidth,
    borderColor: "#bbb",
    width: 30,
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.5
  },
  inputText: {
    padding: 5,
    height: "100%",
    width: "100%"
  },
  checkboxView: {
    marginLeft: 15,
    alignSelf: "flex-start"
  },
  errorView: {
    height: 20
  },
  errorText: {
    color: "red"
  }
});

export const LoginScreen: React.FC<Props> = ({ history }) => {
  const store = React.useContext(RootStoreContext);
  const [showPassword, setshowPassword] = useState(false);
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  const [response, setresponse] = useState<AxiosResponse | null>(null);
  const [errorPresent, setErrorPresent] = useState(false);

  const firstRender = useRef(true);
  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }
    if (response !== undefined) {
      if (response.status === 200) {
        store.loginStore.details = {
          loggedIn: true,
          token: response.data.access_token
        };
        history.push("/");
      } else {
        setErrorPresent(true);
      }
    }
  }, [response]);

  const logIn = async () => {
    setresponse(
      await Request.POST(
        LOGIN_ENDPOINT,
        {
          grant_type: "password",
          client_id: "1",
          client_secret: "Y6YtKraWqUkMEIkgjA06fd4BNDeHeDCnDxseRvnb",
          // 'gHHlCxMmWpjKNILULkGFv7ltAhh2LgZRdsYvUAbz', //localhost
          // 'Y6YtKraWqUkMEIkgjA06fd4BNDeHeDCnDxseRvnb', //cloud
          username: username,
          password: password
        },
        store,
        true
      )
    );
  };

  //TODO - fade error message

  return (
    <ImageBackground
      source={require("../../assets/loginbackground.png")}
      style={styles.container}
    >
      <View style={styles.loginBox}>
        <Image
          source={require("../../assets/logoGoGo.png")}
          style={styles.logo}
        />
        <View style={styles.errorView}>
          <Text style={styles.errorText}>{store.errorStore.error.message}</Text>
        </View>
        <View style={styles.inputContainer}>
          <View style={styles.inputLogo}>
            <Icon size={25} color="#1d1d1d" name="person" />
          </View>
          <TextInput
            placeholder="username"
            onChangeText={text => setusername(text)}
            value={username}
            style={styles.inputText}
          />
        </View>
        <View style={styles.inputContainer}>
          <View style={styles.inputLogo}>
            <Icon size={20} color="#1d1d1d" name="lock" />
          </View>
          <TextInput
            placeholder="password"
            secureTextEntry={!showPassword}
            onChangeText={text => setpassword(text)}
            value={password}
            style={styles.inputText}
            onSubmitEditing={async () => {
              await logIn();
            }}
          />
        </View>
        <CheckBox
          selected={showPassword}
          onPress={() => setshowPassword(!showPassword)}
          text="Show password"
          style={styles.checkboxView}
        />
        <Button
          title="Sign in"
          color="#1e8983"
          onPress={async () => {
            await logIn();
          }}
        />
      </View>
    </ImageBackground>
  );
};

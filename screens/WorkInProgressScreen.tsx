import * as React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { MOBILE_MAX_WIDTH } from "../constants/responsive";

interface Props {
  screenName: string;
}

const styles = StyleSheet.create({
  container: {
    alignContent: "center",
    flexDirection: "column"
  },
  wipImage: {
    width: "100%",
    height: (MOBILE_MAX_WIDTH / 527) * 300,
    aspectRatio: 1,
    marginVertical: 40
  }
});

export const WorkInProgressScreen: React.FC<Props> = ({ screenName }) => {
  return (
    <View style={styles.container}>
      <Image
        resizeMode="stretch"
        source={require("../assets/WIP.png")}
        style={styles.wipImage}
      />
      <Text style={{ textAlign: "center" }}>
        Work in progress, this screen will be replaced with {screenName} after
        the work is done
      </Text>
    </View>
  );
};

import { observable } from "mobx";
import { RootStore } from "./RootStore";

export class LoadingStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @observable loading = false;
}

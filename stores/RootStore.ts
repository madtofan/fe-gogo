import { create } from "mobx-persist";
import { createContext } from "react";
import { AsyncStorage } from "react-native";
import { ErrorStore } from "./ErrorStore";
import { LoadingStore } from "./LoadingStore";
import { LoginStore } from "./LoginStore";
import { RouterStore } from "./RouterStore";

const hydrate = create({
  storage: AsyncStorage,
  jsonify: true
});

export class RootStore {
  loginStore = new LoginStore(this);
  routerStore = new RouterStore(this);
  loadingStore = new LoadingStore(this);
  errorStore = new ErrorStore(this);

  constructor() {
    hydrate("login", this.loginStore);
  }
}

export const RootStoreContext = createContext(new RootStore());

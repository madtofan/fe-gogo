import { observable } from "mobx";
import { persist } from 'mobx-persist';
import { RootStore } from "./RootStore";

interface LoginDetails {
  loggedIn: boolean,
  token: string
}

export class LoginStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @persist('object') @observable details: LoginDetails = {loggedIn: false, token: ""};
}

import { observable } from "mobx";
import { RootStore } from "./RootStore";

export interface errorData {
  status: number;
  message: string;
}

export class ErrorStore {
  rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }
  @observable error: errorData = { message: "", status: 200 };
}

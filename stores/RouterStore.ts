import { observable } from "mobx";
import { ScreenRoutes } from "../constants/screenDetails";
import { RootStore } from "./RootStore";

export class RouterStore {
  rootStore: RootStore;
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }

  @observable screen: ScreenRoutes = "NewsfeedScreen";
}

import * as Font from "expo-font";
import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { setCustomText } from "react-native-global-props";
import { LoadingModal } from "./components/loading_modal/LoadingModal";
import { MainRoutes } from "./components/routes/MainRoutes";
import { RootStoreContext } from "./stores/RootStore";

const App = observer(() => {
  const store = React.useContext(RootStoreContext);
  const [loading, setloading] = useState(true);
  const firstRender = React.useRef(true);
  useEffect(() => {
    async function loadFont() {
      await Font.loadAsync({
        Montserrat: require("./assets/font/Montserrat-Medium.otf"),
        MontserratBold: require("./assets/font/Montserrat-Bold.otf"),
        MontserratThin: require("./assets/font/Montserrat-Thin.otf"),
        MontserratLight: require("./assets/font/Montserrat-Light.otf"),
        MontserratSemiBold: require("./assets/font/Montserrat-SemiBold.otf")
      });
      setloading(false);
    }
    if (firstRender.current) {
      firstRender.current = false;
      loadFont();
      const customTextProps = {
        style: {
          fontFamily: "Montserrat"
        }
      };
      setCustomText(customTextProps);
    }
  });

  if (loading) {
    return <View />;
  } else {
    return (
      <>
        <LoadingModal visible={store.loadingStore.loading} />
        <View
          style={[
            {
              paddingTop: Platform.OS === "web" ? 0 : StatusBar.currentHeight
            },
            styles.main
          ]}
        >
          <MainRoutes />
        </View>
      </>
    );
  }
});

const styles = StyleSheet.create({
  main: {
    width: "100%",
    height: "100%",
    overflow: "hidden"
  }
});

export default App;

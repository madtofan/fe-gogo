import * as React from "react";
import { StyleSheet, View } from "react-native";
import { GLOBAL_STYLE } from "../constants/styling";

interface Props {
  style?: any;
}

const styles = StyleSheet.create({
  card: {
    width: 319,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignSelf: "center",
    margin: 7,
    backgroundColor: "#FFFFFF"
  }
});

export const Card: React.FC<Props> = ({ children, style }) => {
  return (
    <View
      style={[
        styles.card,
        GLOBAL_STYLE.shadow,
        { flexDirection: "column" },
        style
      ]}
    >
      {children}
    </View>
  );
};

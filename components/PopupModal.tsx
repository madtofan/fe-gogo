import * as React from "react";
import { PlatformModal } from "../platform-library/modal";
import { Card } from "./Card";

interface Props {
  popUpData: PopupData;
  setData: (data: PopupData) => void;
}

export interface PopupData {
  openState: boolean;
  xPos: number;
  yPos: number;
}

export const PopupModal: React.FC<Props> = ({
  children,
  popUpData,
  setData
}) => {
  return (
    <PlatformModal
      onBackdropPress={() =>
        setData({
          openState: false,
          xPos: popUpData.xPos,
          yPos: popUpData.yPos
        })
      }
      isVisible={popUpData.openState}
    >
      <Card
        style={[
          {
            position: "absolute",
            left: popUpData.xPos,
            top: popUpData.yPos
          }
        ]}
      >
        {children}
      </Card>
    </PlatformModal>
  );
};

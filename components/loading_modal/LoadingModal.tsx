import * as React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";
import { PlatformModal } from "../../platform-library/modal/index";

interface Props {
  visible: boolean;
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalBackground: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: "#00000040"
  },
  activityIndicatorWrapper: {
    backgroundColor: "#FFFFFF",
    height: 100,
    width: 100,
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    alignSelf: "center"
  }
});

export const LoadingModal: React.FC<Props> = ({ visible }) => {
  return (
    <PlatformModal isVisible={visible}>
      <View style={styles.activityIndicatorWrapper}>
        <ActivityIndicator animating={visible} />
      </View>
    </PlatformModal>
  );
};

import * as React from "react";
import { RouteComponentProps } from "react-router";
import { Route, Switch } from "../../platform-library/router/index";
import { WorkInProgressScreen } from "../../screens/WorkInProgressScreen";

interface Props extends RouteComponentProps {}

export const DetailRoutes: React.FC<Props> = ({ match }) => {
  return (
    <Switch>
      <Route
        exact
        path={`${match.url}/driver`}
        component={WorkInProgressScreen}
      />
      <Route
        exact
        path={`${match.url}/passenger`}
        component={WorkInProgressScreen}
      />
    </Switch>
  );
};

import React from "react";
import { RouteComponentProps } from "react-router";
import { Route, Switch } from "../../platform-library/router/index";
import { HitchScreen } from "../../screens/map/hitch/HitchScreen";
import { WorkInProgressScreen } from "../../screens/WorkInProgressScreen";

interface Props extends RouteComponentProps {}

export const MapRoutes: React.FC<Props> = ({ match }) => {
  return (
    <Switch>
      <Route exact path={`${match.path}/`} component={HitchScreen} />
      <Route exact path={`${match.path}/hitch`} component={HitchScreen} />
      <Route
        exact
        path={`${match.path}/drive`}
        component={WorkInProgressScreen}
      />
    </Switch>
  );
};

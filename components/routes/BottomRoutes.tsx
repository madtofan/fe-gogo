import * as React from "react";
import { StyleSheet, View } from "react-native";
import { Route, Switch } from "../../platform-library/router/index";
import { AddItemButton } from "../../screens/home/progress/AddItemButton";

interface Props {}

const styles = StyleSheet.create({
  floating: {
    position: "absolute",
    bottom: 0
  }
});

export const BottomComponent: React.FC<Props> = () => {
  return (
    <View style={styles.floating}>
      <Switch>
        <Route exact path="/" />
        <Route exact path="/home/progress" component={AddItemButton} />
      </Switch>
    </View>
  );
};

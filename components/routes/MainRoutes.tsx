import { observer } from "mobx-react-lite";
import * as React from "react";
import {
  Redirect,
  Route,
  Router,
  Switch
} from "../../platform-library/router/index";
import { DetailScreen } from "../../screens/detail/DetailScreen";
import { LoginScreen } from "../../screens/login/LoginScreen";
import { MapScreen } from "../../screens/map/MapScreen";
import { RootStoreContext } from "../../stores/RootStore";
import { ProtectedRoute } from "../ProtectedRoute";

interface Props {}

export const MainRoutes: React.FC<Props> = observer(() => {
  const store = React.useContext(RootStoreContext);

  return (
    <Router>
      <Switch>
        {store.loginStore.details.loggedIn ? (
          <Redirect exact from="/" to="/map/hitch" />
        ) : (
          <Redirect exact from="/" to="/login" />
        )}
        <ProtectedRoute
          path="/map"
          component={MapScreen}
          loggedIn={store.loginStore.details.loggedIn}
        />
        <LoginRoute
          path="/login"
          component={LoginScreen}
          loggedIn={store.loginStore.details.loggedIn}
        />
        <ProtectedRoute
          path="/detail"
          component={DetailScreen}
          loggedIn={store.loginStore.details.loggedIn}
        />
      </Switch>
    </Router>
  );
});

const LoginRoute = ({ component: Component, loggedIn, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      !loggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect
          push
          to={{
            pathname: "/map/hitch",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

import * as React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

interface Props {
  selected: boolean;
  onPress: any;
  style?: any;
  textStyle?: any;
  size?: number;
  color?: string;
  text?: string;
}

const styles = StyleSheet.create({
  checkBox: {
    flexDirection: "row",
    alignItems: "center"
  }
});

export const CheckBox: React.FC<Props> = ({
  selected,
  onPress,
  style,
  textStyle,
  size = 15,
  color = "#a4a4a4",
  text = ""
}) => {
  return (
    <TouchableOpacity style={[styles.checkBox, style]} onPress={onPress}>
      <Icon
        size={size}
        color={color}
        name={selected ? "check-box" : "check-box-outline-blank"}
      />

      <Text style={textStyle}> {text} </Text>
    </TouchableOpacity>
  );
};

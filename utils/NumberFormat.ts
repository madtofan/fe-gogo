export const MoneyFormat = (value: number) => {
  return value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
};

export const AddCommas = (value: number) => {
  return value.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, "$&,");
};

import Axios, { AxiosResponse } from "axios";

export const POST = async (
  endpoint: string,
  body: object,
  store: any,
  loading: boolean = false
) => {
  let returnData: AxiosResponse;
  store.loadingStore.loading = loading;
  await Axios.post(endpoint, body)
    .then(response => {
      returnData = response;
    })
    .catch(error => {
      errorParser(error, store);
      returnData = error;
    })
    .finally(() => (store.loadingStore.loading = false));

  return returnData;
};

export interface getParams {
  paramName: string;
  paramValue: any;
}

export const GET = async (
  endpoint: string,
  store: any,
  params: getParams[] = [],
  loading: boolean = false
) => {
  let returnData: AxiosResponse;
  store.loadingStore.loading = loading;
  let requestEndpoint = endpoint;
  if (params.length > 0) {
    let initialChar = "&";
    params.forEach((param, index) => {
      initialChar = "&";
      if (index === 0) {
        initialChar = "?";
      }
      requestEndpoint = requestEndpoint.concat(
        `${initialChar}${param.paramName}=${param.paramValue}`
      );
    });
  }
  await Axios.get(requestEndpoint, {
    headers: { Authorization: `Bearer ${store.loginStore.details.token}` }
  })
    .then(response => {
      returnData = response;
    })
    .catch(error => {
      errorParser(error, store);
      returnData = error;
    })
    .finally(() => (store.loadingStore.loading = false));

  return returnData;
};

const errorParser = (error, store) => {
  if (error.response) {
    if (error.response.status === 401) {
      store.loginStore.details = { loggedIn: false, token: "" };
      store.errorStore.error = {
        status: 401,
        message: "Invalid user / password"
      };
    } else if (error.response.status === 403) {
      store.errorStore.error = {
        status: 403,
        message: "User unauthorized"
      };
    } else {
      store.errorStore.error = {
        status: error.response.status,
        message: error.response.message
      };
    }
  } else {
    store.errorStore.error = {
      status: 0,
      message: "Failed to get any response from the server"
    };
  }
};

const HOST_IP = "ejen2u.penawarglobalfarm.com";
// 'ejen2u.penawarglobalfarm.com';
// '192.168.43.14:8000'

export const PROFILE_ENDPOINT = `http://${HOST_IP}/api/profile`;
export const DASHBOARD_ENDPOINT = `http://${HOST_IP}/api/dashboard`;
export const PROGRESS_ENDPOINT = `http://${HOST_IP}/api/progress`;
export const NOTIFICATION_ENDPOINT = `http://${HOST_IP}/api/notification`;
export const SIDEBAR_ENDPOINT = `http://${HOST_IP}/api/sidebar`;
export const LOGIN_ENDPOINT = `http://${HOST_IP}/oauth/token`;
export const LOGOUT_ENDPOINT = `http://${HOST_IP}/api/logout`;

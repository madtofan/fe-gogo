export type PaidStatus = "Paid" | "Unpaid";
export type ProgressLevel =
  | "Order"
  | "Payment"
  | "Processed"
  | "Delivery"
  | "Completed";
export type FilterType = "View All" | "Date Filter";

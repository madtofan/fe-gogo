interface screenDetail {
  screenName: string;
  screenUrl: string;
  icons: string;
}

export type ScreenRoutes =
  | "AccountBillingScreen"
  | "AccountRegistrationScreen"
  | "AddOrderScreen"
  | "AgentDatabaseScreen"
  | "AgentFamilyTreeScreen"
  | "AuditCommisionScreen"
  | "CheckErrorScreen"
  | "CheckKpiAgentScreen"
  | "ContactHqScreen"
  | "DashboardScreen"
  | "DetailTransactionScreen"
  | "FamilyDirectoryScreen"
  | "FaqScreen"
  | "GamePlanScreen"
  | "MarketingScreen"
  | "NewsfeedScreen"
  | "NotificationScreen"
  | "ProfileScreen"
  | "ProgressScreen"
  | "RecruitSummaryScreen"
  | "RemoveUserScreen"
  | "SalesCommissionScreen"
  | "SalesSummaryScreen"
  | "ScoreRankingScreen"
  | "SetupHofScreen"
  | "SetupAgreementScreen"
  | "SetupContactHqScreen"
  | "SetupFaqScreen"
  | "SetupProductScreen"
  | "SetupRoleScreen"
  | "SetupUpgradeScreen"
  | "SetupWelcomeScreen"
  | "SimpleTransactionScreen"
  | "StatisticScreen"
  | "StockStatusScreen";

export const getScreenDetail = (screen: ScreenRoutes): screenDetail => {
  switch (screen) {
    case "AccountBillingScreen":
      return {
        screenName: "Account & Billing",
        screenUrl: "/home/account-billing",
        icons: "account-balance"
      };
    case "AccountRegistrationScreen":
      return {
        screenName: "Account Registration",
        screenUrl: "/home/account-registration",
        icons: "note-add"
      };
    case "AddOrderScreen":
      return {
        screenName: "Add Order",
        screenUrl: "/order/add-order",
        icons: "note-add"
      };
    case "AgentDatabaseScreen":
      return {
        screenName: "Agent Database",
        screenUrl: "/home/agent-database",
        icons: "dvr"
      };
    case "AgentFamilyTreeScreen":
      return {
        screenName: "Agent Family Tree",
        screenUrl: "/home/agent-family",
        icons: "line-style"
      };
    case "AuditCommisionScreen":
      return {
        screenName: "Audit Commision",
        screenUrl: "/home/audit-commision",
        icons: "assignment"
      };
    case "CheckErrorScreen":
      return {
        screenName: "Check Error",
        screenUrl: "/home/check-error",
        icons: "assignment-late"
      };
    case "CheckKpiAgentScreen":
      return {
        screenName: "Check KPI Agent",
        screenUrl: "/home/check-kpi-agent",
        icons: "assignment-ind"
      };
    case "ContactHqScreen":
      return {
        screenName: "Contact HQ",
        screenUrl: "/home/contact-hq",
        icons: "headset-mic"
      };
    case "DashboardScreen":
      return {
        screenName: "Dashboard",
        screenUrl: "/home/dashboard",
        icons: "dashboard"
      };
    case "DetailTransactionScreen":
      return {
        screenName: "Detail Transaction",
        screenUrl: "/home/detail-transaction",
        icons: "receipt"
      };
    case "FamilyDirectoryScreen":
      return {
        screenName: "Family Directory",
        screenUrl: "/home/family-directory",
        icons: "device-hub"
      };
    case "FaqScreen":
      return {
        screenName: "FAQ",
        screenUrl: "/home/faq",
        icons: "question-answer"
      };
    case "GamePlanScreen":
      return {
        screenName: "Game Plan",
        screenUrl: "/home/game-plan",
        icons: "important-devices"
      };
    case "MarketingScreen":
      return {
        screenName: "Marketing",
        screenUrl: "/home/marketing",
        icons: "speaker-notes"
      };
    case "NewsfeedScreen":
      return {
        screenName: "News Feed",
        screenUrl: "/home/newsfeed",
        icons: "rss-feed"
      };
    case "NotificationScreen":
      return {
        screenName: "Notification",
        screenUrl: "/home/notification",
        icons: "notifications-active"
      };
    case "ProfileScreen":
      return {
        screenName: "Profile",
        screenUrl: "/home/profile",
        icons: "person-outline"
      };
    case "ProgressScreen":
      return {
        screenName: "Progress",
        screenUrl: "/home/progress",
        icons: "local-shipping"
      };
    case "RecruitSummaryScreen":
      return {
        screenName: "Recruit Summary",
        screenUrl: "/home/recruit-summary",
        icons: "group-add"
      };
    case "RemoveUserScreen":
      return {
        screenName: "Remove User",
        screenUrl: "/home/remove-user",
        icons: "delete"
      };
    case "SalesCommissionScreen":
      return {
        screenName: "Sales Commision",
        screenUrl: "/home/sales-commision",
        icons: "card-membership"
      };
    case "SalesSummaryScreen":
      return {
        screenName: "Sales Summary",
        screenUrl: "/home/sales-summary",
        icons: "chrome-reader-mode"
      };
    case "ScoreRankingScreen":
      return {
        screenName: "Score Ranking",
        screenUrl: "/home/score-ranking",
        icons: "star-border"
      };
    case "SetupHofScreen":
      return {
        screenName: "Setup Hall Of Fame",
        screenUrl: "/home/setup-hof",
        icons: "settings"
      };
    case "SetupAgreementScreen":
      return {
        screenName: "Setup Agreement",
        screenUrl: "/home/setup-agreement",
        icons: "settings"
      };
    case "SetupContactHqScreen":
      return {
        screenName: "Setup Contact HQ",
        screenUrl: "/home/setup-contact-hq",
        icons: "settings"
      };
    case "SetupFaqScreen":
      return {
        screenName: "Setup FAQ",
        screenUrl: "/home/setup-faq",
        icons: "settings"
      };
    case "SetupProductScreen":
      return {
        screenName: "Setup Product",
        screenUrl: "/home/setup-product",
        icons: "settings"
      };
    case "SetupRoleScreen":
      return {
        screenName: "Setup Role",
        screenUrl: "/home/setup-role",
        icons: "settings"
      };
    case "SetupUpgradeScreen":
      return {
        screenName: "Setup Upgrade",
        screenUrl: "/home/setup-upgrade",
        icons: "settings"
      };
    case "SetupWelcomeScreen":
      return {
        screenName: "Setup Welcome",
        screenUrl: "/home/setup-welcome",
        icons: "settings"
      };
    case "SimpleTransactionScreen":
      return {
        screenName: "Simple Transaction",
        screenUrl: "/home/simple-transaction",
        icons: "settings"
      };
    case "StatisticScreen":
      return {
        screenName: "Statistic",
        screenUrl: "/home/statistic",
        icons: "insert-chart"
      };
    case "StockStatusScreen":
      return {
        screenName: "Stock Status",
        screenUrl: "/home/stock-status",
        icons: "store"
      };
  }
};

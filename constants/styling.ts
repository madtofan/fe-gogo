import { StyleSheet } from "react-native";
export const GLOBAL_STYLE = StyleSheet.create({
  shadow: {
    shadowOpacity: 0.2,
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 2 },
    shadowRadius: 3,
    elevation: 3,
    zIndex: 999
  },
  sidebarButton: {
    marginVertical: 2,
    marginHorizontal: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 4
  },
  faIcons: {
    marginLeft: "auto",
    marginRight: 5
  },
  textShadow: {
    textShadowColor: "rgba(0, 0, 0, 0.25)",
    textShadowOffset: { width: 2, height: 4 },
    textShadowRadius: 1
  }
});

import Modal from "modal-enhanced-react-native-web";
import * as React from "react";

interface Props {
  isVisible: boolean;
  onBackdropPress?: any;
}

export const PlatformModal: React.FC<Props> = ({
  isVisible,
  children,
  onBackdropPress
}) => {
  return (
    <Modal
      onBackdropPress={onBackdropPress}
      isVisible={isVisible}
      animationIn={"fadeIn"}
      animationOut={"fadeOut"}
    >
      {children}
    </Modal>
  );
};

import * as React from "react";
import Modal from "react-native-modal";

interface Props {
  isVisible: boolean;
  onBackdropPress?: any;
}

export const PlatformModal: React.FC<Props> = ({
  isVisible,
  onBackdropPress,
  children
}) => {
  return (
    <Modal
      onBackdropPress={onBackdropPress}
      isVisible={isVisible}
      animationIn={"fadeIn"}
      animationOut={"fadeOut"}
    >
      {children}
    </Modal>
  );
};

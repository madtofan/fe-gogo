export {
  NativeRouter as Router,
  Redirect,
  Route,
  Switch,
  withRouter
} from "react-router-native";
